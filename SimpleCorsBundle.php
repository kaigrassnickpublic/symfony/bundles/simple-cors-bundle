<?php declare(strict_types=1);

namespace KaiGrassnick\SimpleCorsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SimpleCorsBundle
 *
 * @package KaiGrassnick\SimpleCorsBundle
 */
class SimpleCorsBundle extends Bundle
{

}