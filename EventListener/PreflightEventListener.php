<?php declare(strict_types=1);
/*******************************************************************************
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleCorsBundle\EventListener;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

/**
 * Class PreflightEventListener
 *
 * @package KaiGrassnick\SimpleCorsBundle\EventListener
 */
class PreflightEventListener
{

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        if ($event->getRequestType() != HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        if ($event->getRequest()->getMethod() !== Request::METHOD_OPTIONS) {
            return;
        }

        $throwable = $event->getThrowable();
        if (!$throwable instanceof MethodNotAllowedHttpException) {
            return;
        }

        $previousThrowable = $throwable->getPrevious();
        if (!$previousThrowable instanceof MethodNotAllowedException) {
            return;
        }

        $requestedMethod = $event->getRequest()->headers->get('access-control-request-method', '');
//        $requestedHeaders = $event->getRequest()->headers->get('access-control-request-headers', '');
        $allowedHeaders = array_merge($event->getRequest()->headers->keys(), ["authorization"]);
        $allowedMethods  = $previousThrowable->getAllowedMethods();

        $headers = implode(", ", $allowedHeaders);
        $methods = implode(", ", $allowedMethods);

        $content    = null;
        $statusCode = Response::HTTP_NO_CONTENT;

        if (!in_array($requestedMethod, $allowedMethods)) {
            $statusCode = $throwable->getStatusCode();

            $contentArray = [
                "type"   => "https://tools.ietf.org/html/rfc2616#section-10",
                "title"  => "An error occurred",
                "status" => $statusCode,
                "detail" => sprintf("Method Not Allowed (Allow: %s)", $methods),
            ];

            $content = json_encode($contentArray);
        }

        $response = new Response($content, $statusCode);

        $response->headers->set('Access-Control-Allow-Methods', $methods);
        $response->headers->set('Access-Control-Allow-Headers', '*');
        $response->headers->set('Access-Control-Max-Age', 3600);

        $event->setResponse($response);
        $event->allowCustomResponseCode();
    }
}
