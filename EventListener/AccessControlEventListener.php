<?php declare(strict_types=1);
/*******************************************************************************
 * Author: Kai Grassnick <info@kai-grassnick.de>
 ******************************************************************************/

namespace KaiGrassnick\SimpleCorsBundle\EventListener;


use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class AccessControlEventListener
 *
 * @package KaiGrassnick\SimpleCorsBundle\EventListener
 */
class AccessControlEventListener
{

    /**
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        $event->getResponse()->headers->set('Access-Control-Allow-Origin', '*');
    }
}
