# Simple CORS Bundle

This Bundle provides simple CORS features like Preflight handling.

For now, this bundle is limited in functionality. We allow all origins and all request methods.
This is about to be changed in future releases.